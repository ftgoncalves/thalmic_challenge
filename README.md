## Description

For this challenge I used [RxJava](https://github.com/ReactiveX/RxJava) and framework [Fluent](https://github.com/fluentio/Fluent)

The advantage of using fluent with RxJava is that the view state is only modified in one place
and in the best location for it, and View is responsible for shadowing its state.


The fluent framework is a very simple framework where your responsibility is to organize the data flow of an application
where all data is unidirectional

Example:
Activity.event -> Hub -> Job -> State

and state modification is optional.

## How to Play

The player must start a new game with the button "NEW GAME"

There are 4 buttons at the bottom of the screen with the initial blue color,
clicking the button will change its color to the color the player wants.

After selecting the desired color set it is necessary to click on "PLAY"
for the computer to check if your game is correct


