package com.ftgoncalves.thalmicchallenge.jobs

import com.ftgoncalves.thalmicchallenge.utils.TestSchedulerRule
import com.ftgoncalves.thalmicchallenge.GameState
import com.ftgoncalves.thalmicchallenge.GameStateType
import io.fluent.rx.RxStore
import org.junit.Assert.assertTrue
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class StartGameJobTest {

    @Spy
    private val store = RxStore(GameState())

    @InjectMocks
    lateinit var startGameJob: StartGameJob

    @Test
    fun shouldSetStateTypeToReadyAndCreateAGoal() {
        val testStartGameJob = startGameJob.bind(Unit).test()
        testScheduler.triggerActions()
        testStartGameJob.assertComplete()

        store.stateChanges().test()
                .assertValue { it.type == GameStateType.Ready }
                .assertValue { it.goal != null }
                .assertValue { it.goal!!.colors.size == 4 }
    }

    @Test
    fun shouldCreateDifferencesGoals() {
        startGameJob.bind(Unit).test()
        testScheduler.triggerActions()

        val storeTest = store.stateChanges().test()
                .assertValueCount(1)

        startGameJob.bind(Unit).test()
        testScheduler.triggerActions()

        storeTest.assertValueCount(2)
        val states = storeTest.values()

        assertTrue(states[0].goal!!.colors != states[1].goal!!.colors)
    }

    companion object {
        @ClassRule
        @JvmField
        val testSchedulerRule = TestSchedulerRule()
        val testScheduler = testSchedulerRule.testScheduler
    }
}