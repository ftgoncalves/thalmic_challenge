package com.ftgoncalves.thalmicchallenge.jobs

import com.ftgoncalves.thalmicchallenge.GameState
import com.ftgoncalves.thalmicchallenge.GameStateType
import com.ftgoncalves.thalmicchallenge.model.Bet
import com.ftgoncalves.thalmicchallenge.model.Color
import com.ftgoncalves.thalmicchallenge.model.Pegs
import com.ftgoncalves.thalmicchallenge.utils.TestSchedulerRule
import io.fluent.rx.RxStore
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BetJobTest {

    @Spy
    private val store = RxStore(GameState())

    @InjectMocks
    lateinit var betJob: BetJob

    @Test
    fun shouldDispatchOnePegToTheStoreWithStateTypeAgain() {
        val goal = Pegs(listOf(Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE))
        store.update { newGame(goal) }

        val bet = Pegs(listOf(Color.RED, Color.RED, Color.BLUE, Color.BLUE))

        val testBetJob = betJob.bind(bet).test()
        testScheduler.triggerActions()
        testBetJob.assertComplete()

        store.stateChanges().test()
                .assertValue { it.pegs.size == 1 }
                .assertValue { it.pegs[0].pegs!!.colors == bet.colors }
                .assertValue { it.type == GameStateType.Again }
    }

    @Test
    fun shouldValidateTheRulesOfRightPositionAndRightColor() {
        val goal = Pegs(listOf(Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE))
        store.update { newGame(goal) }

        val bet = Pegs(listOf(Color.RED, Color.RED, Color.BLUE, Color.BLUE))

        val testBetJob = betJob.bind(bet).test()
        testScheduler.triggerActions()
        testBetJob.assertComplete()

        store.stateChanges().test()
                .assertValue { it.pegs[0].rightColor == 2 }
                .assertValue { it.pegs[0].rightPosition == 2 }
    }

    @Test
    fun shouldDispatchStateTypeLoseForTheLastWrongTry() {
        val goal = Pegs(listOf(Color.RED, Color.RED, Color.RED, Color.RED))

        store.update { newGame(goal) }

        for (i in 1..9) {
            val peg = Pegs(listOf(Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE))
            val bet = Bet(peg, 0, 0)
            store.update { addPegs(bet, GameStateType.Again) }
        }

        val peg = Pegs(listOf(Color.PINK, Color.PINK, Color.PINK, Color.PINK))

        val testBetJob = betJob.bind(peg).test()
        testScheduler.triggerActions()

        testBetJob.assertComplete()

        store.stateChanges().test()
                .assertValue { it.type == GameStateType.Lose }
    }

    @Test
    fun shouldDispatchStateTypeWinForTheLastTry() {
        val goal = Pegs(listOf(Color.PINK, Color.PURPLE, Color.RED, Color.BLUE))

        store.update { newGame(goal) }

        for (i in 1..9) {
            val peg = Pegs(listOf(Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE))
            val bet = Bet(peg, 0, 0)
            store.update { addPegs(bet, GameStateType.Again) }
        }

        val peg = Pegs(listOf(Color.PINK, Color.PURPLE, Color.RED, Color.BLUE))

        val testBetJob = betJob.bind(peg).test()
        testScheduler.triggerActions()

        testBetJob.assertComplete()

        store.stateChanges().test()
                .assertValue { it.type == GameStateType.Win }
    }

    companion object {
        @ClassRule
        @JvmField
        val testSchedulerRule = TestSchedulerRule()
        val testScheduler = testSchedulerRule.testScheduler
    }
}
