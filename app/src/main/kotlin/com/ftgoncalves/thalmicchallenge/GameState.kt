package com.ftgoncalves.thalmicchallenge

import com.ftgoncalves.thalmicchallenge.model.Bet
import com.ftgoncalves.thalmicchallenge.model.Pegs
import io.fluent.State
import io.fluent.StateType

data class GameState(
        val type: StateType = StateType.Initial,
        val goal: Pegs? = null,
        val pegs: List<Bet> = emptyList()
) : State {

    override fun type() = type

    fun newGame(pegs: Pegs) = copy(
            type = GameStateType.Ready,
            goal = pegs,
            pegs = emptyList()
    )

    fun addPegs(guess: Bet, type: StateType): GameState {
        val pegsFinal = mutableListOf<Bet>()
        pegsFinal.addAll(pegs)
        pegsFinal.add(guess)
        return copy(pegs = pegsFinal, type = type)
    }
}

sealed class GameStateType : StateType() {
    object Win : GameStateType()
    object Lose : GameStateType()
    object Again : GameStateType()
    object Ready : GameStateType()
}
