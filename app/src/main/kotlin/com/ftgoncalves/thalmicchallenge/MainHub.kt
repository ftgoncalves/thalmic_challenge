package com.ftgoncalves.thalmicchallenge

import com.ftgoncalves.thalmicchallenge.model.Pegs
import com.ftgoncalves.thalmicchallenge.view.MainView
import io.fluent.rx.RxHub
import io.fluent.rx.RxJob
import javax.inject.Inject

/*
 * For each view there will only be one hub
 */
class MainHub @Inject constructor(
        private val betJob: RxJob<@JvmSuppressWildcards Pegs>,
        private val startGameJob: RxJob<@JvmSuppressWildcards Unit>
) : RxHub<MainView>() {

    /*
     * Within the Fluent framework bind already does the subscribe,
     * the association of the correct job and adds to the correct disposable management.
     */
    override fun connect(view: MainView) {
        view.newGame().bind(startGameJob)
        view.bet().bind(betJob)
    }
}
