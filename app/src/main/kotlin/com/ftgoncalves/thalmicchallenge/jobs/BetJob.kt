package com.ftgoncalves.thalmicchallenge.jobs

import com.ftgoncalves.thalmicchallenge.GameState
import com.ftgoncalves.thalmicchallenge.GameStateType
import com.ftgoncalves.thalmicchallenge.model.Bet
import com.ftgoncalves.thalmicchallenge.model.Pegs
import io.fluent.rx.RxJob
import io.fluent.rx.RxStore
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/*
 * Since the job is only responsible for a task, it is necessary to add a more expressive name.
 */
class BetJob @Inject constructor(
        private val store: RxStore<GameState>
) : RxJob<Pegs>() {

    override fun bind(input: Pegs): Completable {
        return Single.fromCallable { store.state() }
                .observeOn(Schedulers.computation())
                .flatMap { createBet(input, it.goal!!) }
                .flatMap { stateOfTheGame(it, store.state().pegs.size) }
                .doOnSuccess { store.update { addPegs(it.first, it.second) } }
                .toCompletable()
    }

    private fun stateOfTheGame(bet: Bet, betCount: Int): Single<Pair<Bet, GameStateType>> {
        return Single.fromCallable {
            if (bet.rightColor == 4 && bet.rightPosition == 4) {
                Pair(bet, GameStateType.Win)
            } else {
                if (betCount == 9) {
                    Pair(bet, GameStateType.Lose)
                } else {
                    Pair(bet, GameStateType.Again)
                }
            }
        }
    }

    private fun createBet(bet: Pegs, goal: Pegs): Single<Bet> {
        return Single.fromCallable {
            val rightPositionAndRightColor = getMatchForRightPositionAndRightColor(bet, goal)

            Bet(pegs = bet,
                    rightPosition = rightPositionAndRightColor.first,
                    rightColor = rightPositionAndRightColor.second
            )
        }
    }

    private fun getMatchForRightPositionAndRightColor(bet: Pegs, goal: Pegs): Pair<Int, Int> {
        var rightPosition = 0
        var rightColor = 0

        val betCopy = bet.colors.toMutableList()

        for ((i, inputPeg) in goal.colors.withIndex()) {
            if (bet.colors[i] == inputPeg) {
                rightPosition++
            }

            for ((e, storePeg) in betCopy.withIndex()) {
                if (inputPeg == storePeg) {
                    rightColor++
                    betCopy.removeAt(e)
                    break
                }
            }
        }

        return Pair(rightPosition, rightColor)
    }
}
