package com.ftgoncalves.thalmicchallenge.jobs

import com.ftgoncalves.thalmicchallenge.GameState
import com.ftgoncalves.thalmicchallenge.model.Color
import com.ftgoncalves.thalmicchallenge.model.Pegs
import io.fluent.rx.RxJob
import io.fluent.rx.RxStore
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/*
 * Since the job is only responsible for a task, it is necessary to add a more expressive name.
 */
class StartGameJob @Inject constructor(
        private val store: RxStore<GameState>
) : RxJob<Unit>() {

    override fun bind(input: Unit): Completable {
        return Single.fromCallable { store.state() }
                .observeOn(Schedulers.computation())
                .flatMap { generateNewGoalPeg() }
                .doOnSuccess { store.update { newGame(it) } }
                .toCompletable()
    }

    private fun generateNewGoalPeg(): Single<Pegs> {
        val colors = Color.values()

        return Observable.fromCallable { (0 until colors.size).random() }
                .repeat()
                .map { colors[it] }
                .take(4)
                .toList()
                .flatMap { Single.just(Pegs(it)) }
    }

    private fun ClosedRange<Int>.random() =
            Random().nextInt((endInclusive + 1) - start) +  start
}
