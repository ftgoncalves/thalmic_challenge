package com.ftgoncalves.thalmicchallenge.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.ftgoncalves.thalmicchallenge.GameState
import com.ftgoncalves.thalmicchallenge.GameStateType
import com.ftgoncalves.thalmicchallenge.R
import com.ftgoncalves.thalmicchallenge.model.Pegs
import com.jakewharton.rxbinding2.view.RxView
import dagger.android.support.DaggerAppCompatActivity
import io.fluent.StateType
import io.fluent.rx.RxHub
import io.fluent.rx.RxStore
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainView {

    @Inject
    lateinit var hub: RxHub<@JvmSuppressWildcards MainView>

    @Inject
    lateinit var store: RxStore<GameState>

    private val pegAdapter by lazy {
        PegsAdapter(this)
    }

    override fun bet(): Observable<Pegs> = RxView.clicks(play)
            .map { generatePegs() }

    override fun newGame(): Observable<Unit> = RxView.clicks(newGame)
            .map { Unit }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        hub.connect(this)

        store.stateChanges()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { bind(it) }

        pegs.layoutManager = LinearLayoutManager(this)
        pegs.setHasFixedSize(true)
        pegs.adapter = pegAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        hub.disconnect()
    }

    override fun bind(newState: GameState) {
        when (newState.type) {
            GameStateType.Again -> {
                addItem(newState)
            }
            GameStateType.Ready -> {
                Log.d("DEBUG", newState.goal?.colors.toString())
                play.isEnabled = true
                resetGame()
            }
            StateType.Initial -> {
                play.isEnabled = false
            }
            GameStateType.Lose -> {
                addItem(newState)

                play.isEnabled = false
                Toast.makeText(this, "You lose! Play again", Toast.LENGTH_LONG).show()
            }
            GameStateType.Win -> {
                addItem(newState)
                play.isEnabled = false
                Toast.makeText(this, "Congratulations! You win", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun addItem(newState: GameState) {
        val position = newState.pegs.size - 1
        pegAdapter.addBet(newState.pegs[position], position)
    }

    private fun resetGame() {
        Toast.makeText(this, "Go", Toast.LENGTH_LONG).show()
        pegAdapter.reset()
        color1.reset()
        color2.reset()
        color3.reset()
        color4.reset()
    }

    private fun generatePegs(): Pegs {
        val peg = listOf(
                color1.getSelectedColor(),
                color2.getSelectedColor(),
                color3.getSelectedColor(),
                color4.getSelectedColor()
        )
        return Pegs(peg)
    }
}
