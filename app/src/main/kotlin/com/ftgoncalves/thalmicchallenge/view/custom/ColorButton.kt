package com.ftgoncalves.thalmicchallenge.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import com.ftgoncalves.thalmicchallenge.model.Color

class ColorButton @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0
) : Button(context, attrs, defStyle) {

    private var selectedColor: Int = 0

    fun getSelectedColor() = colors[selectedColor]

    fun reset() {
        selectedColor = 0
        setBackgroundResource(colors[selectedColor].colorDef)
    }

    private val colors = Color.values()

    init {
        setBackgroundResource(colors[selectedColor].colorDef)

        var index = 0
        setOnClickListener {
            if (index == (colors.size - 1)) {
                index = 0
            } else {
                index++
            }

            selectedColor = index
            setBackgroundResource(colors[selectedColor].colorDef)
        }
    }
}
