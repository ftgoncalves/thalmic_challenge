package com.ftgoncalves.thalmicchallenge.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ftgoncalves.thalmicchallenge.R
import com.ftgoncalves.thalmicchallenge.model.Bet

class PegsAdapter constructor(
        private val context: Context
) : RecyclerView.Adapter<PegsAdapter.PegViewHolder>() {

    private var bets: MutableList<Bet> = emptyBet()

    fun addBet(bet: Bet, position: Int) {
        bets[position] = bet
        notifyItemChanged(position)
    }

    fun reset() {
        bets = emptyBet()

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PegViewHolder {
        val item = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_peg, parent, false)
        return PegViewHolder(item)
    }

    override fun getItemCount() = bets.size

    override fun onBindViewHolder(holder: PegViewHolder, position: Int) {
        holder.bind(bets[position], position + 1)
    }

    private fun emptyBet() = MutableList(10) { Bet(null, 0, 0) }

    inner class PegViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(bet: Bet, item: Int) {
            if (bet.pegs != null) {
                bet.pegs.let {
                    itemView.findViewById<View>(R.id.color1).setBackgroundResource(it.colors[0].colorDef)
                    itemView.findViewById<View>(R.id.color2).setBackgroundResource(it.colors[1].colorDef)
                    itemView.findViewById<View>(R.id.color3).setBackgroundResource(it.colors[2].colorDef)
                    itemView.findViewById<View>(R.id.color4).setBackgroundResource(it.colors[3].colorDef)
                    itemView.findViewById<TextView>(R.id.feedback).text =
                            context.getString(R.string.feedback, bet.rightPosition, bet.rightColor)
                }
            } else {
                itemView.findViewById<View>(R.id.color1).setBackgroundResource(R.color.black)
                itemView.findViewById<View>(R.id.color2).setBackgroundResource(R.color.black)
                itemView.findViewById<View>(R.id.color3).setBackgroundResource(R.color.black)
                itemView.findViewById<View>(R.id.color4).setBackgroundResource(R.color.black)
                itemView.findViewById<TextView>(R.id.feedback).text = ""
            }

            itemView.findViewById<TextView>(R.id.item).text = item.toString()
        }
    }
}
