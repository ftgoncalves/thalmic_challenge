package com.ftgoncalves.thalmicchallenge.view

import com.ftgoncalves.thalmicchallenge.GameState
import com.ftgoncalves.thalmicchallenge.model.Pegs
import io.fluent.View
import io.reactivex.Observable

interface MainView : View<GameState> {
    fun bet(): Observable<Pegs>
    fun newGame(): Observable<Unit>
}
