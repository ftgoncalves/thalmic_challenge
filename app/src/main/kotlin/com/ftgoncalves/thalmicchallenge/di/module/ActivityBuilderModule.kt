package com.ftgoncalves.thalmicchallenge.di.module

import com.ftgoncalves.thalmicchallenge.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

  @ActivityScoped
  @ContributesAndroidInjector(modules = [(MainModule::class)])
  internal abstract fun mainActivity(): MainActivity

}
