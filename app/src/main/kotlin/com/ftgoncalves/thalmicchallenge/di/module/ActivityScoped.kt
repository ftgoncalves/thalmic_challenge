package com.ftgoncalves.thalmicchallenge.di.module

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScoped
