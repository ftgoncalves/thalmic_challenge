package com.ftgoncalves.thalmicchallenge.di.module

import com.ftgoncalves.thalmicchallenge.GameState
import com.ftgoncalves.thalmicchallenge.MainHub
import com.ftgoncalves.thalmicchallenge.jobs.BetJob
import com.ftgoncalves.thalmicchallenge.jobs.StartGameJob
import com.ftgoncalves.thalmicchallenge.model.Pegs
import com.ftgoncalves.thalmicchallenge.view.MainActivity
import com.ftgoncalves.thalmicchallenge.view.MainView
import dagger.Module
import dagger.Provides
import io.fluent.rx.RxHub
import io.fluent.rx.RxJob
import io.fluent.rx.RxStore

@Module
class MainModule {

    @Provides
    fun providesMainActivity(activity: MainActivity): MainView = activity

    @Provides
    @ActivityScoped
    fun providesGameStore() = RxStore(GameState())

    @Provides
    fun providesMainHub(mainHub: MainHub): RxHub<MainView> = mainHub

    @Provides
    fun providesBetJob(betJob: BetJob): RxJob<Pegs> = betJob

    @Provides
    fun providesStartGameJob(startGameJob: StartGameJob): RxJob<Unit> = startGameJob
}
