package com.ftgoncalves.thalmicchallenge.di.component

import android.app.Application
import com.ftgoncalves.thalmicchallenge.MastermindApplication
import com.ftgoncalves.thalmicchallenge.di.module.ActivityBuilderModule
import com.ftgoncalves.thalmicchallenge.di.module.ApplicationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    ActivityBuilderModule::class
])
interface ApplicationComponent : AndroidInjector<MastermindApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }
}
