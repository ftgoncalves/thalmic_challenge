package com.ftgoncalves.thalmicchallenge.model


data class Pegs(
        val colors: List<Color>
)

data class Bet(
        val pegs: Pegs?,
        val rightPosition: Int,
        val rightColor: Int
)
