package com.ftgoncalves.thalmicchallenge.model

import com.ftgoncalves.thalmicchallenge.R

enum class Color(val colorDef: Int) {
    BLUE(R.color.blue),
    RED(R.color.red),
    GREEN(R.color.green),
    YELLOW(R.color.yellow),
    PURPLE(R.color.purple),
    PINK(R.color.pink);
}
