package com.ftgoncalves.thalmicchallenge

import com.ftgoncalves.thalmicchallenge.di.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class MastermindApplication: DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder()
                .application(this)
                .build()
                .apply { inject(this@MastermindApplication) }
    }
}